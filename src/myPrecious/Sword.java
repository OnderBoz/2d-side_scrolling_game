package myPrecious;

import java.awt.*;

import javax.swing.ImageIcon;

public class Sword {
	
	int x, y;
	boolean isVisible = true;
	Image img;
	
	
	public Sword(int startX, int startY)
	{
		
		x = startX;
		y = startY;
		ImageIcon newSword = new ImageIcon("images/sword.png");
		img = newSword.getImage();
		isVisible = true;
			
	}
	
	public Rectangle getBound(){
		
		return new Rectangle(x, y, 64, 64);
		
	}
	
	public int getX(){
		
		return x;
	}
	
	public int getY(){
		
		return y;
	}
	
	public Image getImage(){
		
		return img;
	}
	
	public boolean getVisible(){
		
		return isVisible;
	}
	
	public void move(){
		
		x = x + 1;
		
		if(x > 1253)
			isVisible = false;
	}
	
}

