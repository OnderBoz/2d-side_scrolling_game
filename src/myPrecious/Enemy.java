package myPrecious;

import java.awt.*;

import javax.swing.ImageIcon;

public class Enemy {
	
	Image img;
	int x,y;
	boolean isAlive = true;
	int width, length;
	
	
	public Enemy(int startX, int startY, int wid, int len, String location){
		
		x = startX;
		y = startY;
		width = wid;
		length = len;
		ImageIcon img2 = new ImageIcon(location);
		img = img2.getImage();
		
	}
	
	public int getX(){
		
		return x;
	}
	
	public int getY(){
		
		return y;
	}
	
	
	public boolean getAlive(){
		
		return isAlive;
	}
	
	public Image getImage(){
		
		return img;
	}
	
	public Rectangle getBound(){
		
		return new Rectangle(x, y, width, length);
		
	}
	
	
	public void move(int dx, int left){
		if(dx == 2 && !((left + dx) < 350) )
			x = x - dx;
	}
}
