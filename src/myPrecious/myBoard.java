package myPrecious;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import javax.swing.*;

public class myBoard extends JPanel implements ActionListener{

	Precious myring;
	public Image myimg;
	Timer mytime;
	
	boolean gameOver = false;
	boolean youWon = false;
	
	Enemy enemy1;
	Enemy enemy2;
	Enemy enemy3;
	Enemy enemy4;
	Enemy enemy5;
	Enemy enemy6;
	
		public myBoard(){
			
			myring = new Precious(); 
			addKeyListener(new AL());
			setFocusable(true);    //to move left or right when i press the keys.
			ImageIcon img = new ImageIcon("images/background.png");
			myimg = img.getImage();
			mytime = new Timer(2, this); //to update my image
			mytime.start();
			
			enemy1 = new Enemy(1253, 150, 101, 100, "images/enemy.png");
			enemy2 = new Enemy(1453, 100, 101, 100, "images/enemy.png");
			enemy3 = new Enemy(1853, 50, 101, 100, "images/enemy.png");
			enemy4 = new Enemy(2253, 200, 89, 216, "images/enemy2.png");
			enemy5 = new Enemy(2653, 200, 110, 162, "images/enemy1.png");
			enemy6 = new Enemy(2853, 200, 189, 280, "images/enemy3.png");
		}
		
		public void actionPerformed(ActionEvent e){
			
			
			try{
			checkCollision();		
			}
			catch(Exception np){
				
				
				
			}
			
			
			
			//System.out.println(myring.getX()+ " " + enemy1.getX());
			
			ArrayList swords = Precious.getSwords();
			
			for(int i = 0; i < swords.size(); i++){
				
				Sword xy = (Sword) swords.get(i);
				
				if(xy.getVisible() == true)
					xy.move();
				else
					swords.remove(xy);
			}
			
			myring.move();
			
			
			try{
			enemy1.move(myring.getdx(), myring.getLeft());
			
			enemy2.move(myring.getdx(), myring.getLeft());
			
			enemy3.move(myring.getdx(), myring.getLeft());
			
			enemy4.move(myring.getdx(), myring.getLeft());
			
			enemy5.move(myring.getdx(), myring.getLeft());
			
			enemy6.move(myring.getdx(), myring.getLeft());
			}
			catch(Exception np){
			
			}
			
			
			
			repaint();
		}
		
		public void checkCollision(){
			
			Rectangle rec1 = enemy1.getBound();
			
			Rectangle rec2 = enemy2.getBound();
			
			Rectangle rec3 = enemy3.getBound();
			
			Rectangle rec4 = enemy4.getBound();
			
			Rectangle rec5 = enemy5.getBound();
			
			Rectangle rec6 = enemy6.getBound();
			
			ArrayList swords = Precious.getSwords();
			
			for(int i = 0; i < swords.size(); i++){
				Sword xyz = (Sword) swords.get(i);
				Rectangle r1 = xyz.getBound();
				if(rec1.intersects(r1) && enemy1.isAlive){
					enemy1.isAlive = false;
					xyz.isVisible = false;
				}
				else if(rec2.intersects(r1) && enemy2.isAlive){
					enemy2.isAlive = false;
					xyz.isVisible = false;
				}
				else if(rec3.intersects(r1) && enemy3.isAlive){
					enemy3.isAlive = false;
					xyz.isVisible = false;
				}
				else if(rec4.intersects(r1) && enemy4.isAlive){
					enemy4.isAlive = false;
					xyz.isVisible = false;
				}
				else if(rec5.intersects(r1) && enemy5.isAlive){
					enemy5.isAlive = false;
					xyz.isVisible = false;
				}
				else if(rec6.intersects(r1) && enemy6.isAlive){
					enemy6.isAlive = false;
					xyz.isVisible = false;
				}
					
				
			}
			
			Rectangle ring = myring.getBound();
			
			if((ring.intersects(rec1) && enemy1.isAlive) || (ring.intersects(rec2)&& enemy2.isAlive) || (ring.intersects(rec3)&& enemy3.isAlive) || (ring.intersects(rec4)&& enemy4.isAlive) || (ring.intersects(rec5)&& enemy5.isAlive) || (ring.intersects(rec6)&& enemy6.isAlive))
				gameOver = true;
			
			if(!(enemy1.isAlive) && !(enemy2.isAlive) && (!enemy3.isAlive) && (!enemy4.isAlive) && (!enemy5.isAlive) && (!enemy6.isAlive))
				youWon = true;
			
			
		}
		
		public void paint(Graphics g){
			
			if(gameOver){
				System.out.println("GAME OVER");
				System.exit(0);
			}
			
			if(youWon){
				System.out.println("YOU WON");
				System.exit(0);
			}
			
			
			
			
			
			
			super.paint(g);
			Graphics2D g2d = (Graphics2D) g;
			
			if(((myring.getX() - 75)  % 2500) == 0){
				myring.bgx2 = 0;
			}
			
			if(((myring.getX() - 1335) % 2500) == 0 ){
				myring.bgx = 0;
				
			}
			
			g2d.drawImage(myimg, (1240-myring.bgx), 0, null);
			
			if(myring.getX() >= 75){
				g2d.drawImage(myimg, (1240-myring.bgx2), 0, null);
			}
			
			g2d.drawImage(myring.getImage(), myring.leftBound, myring.getY(), null);
			
			ArrayList swords = Precious.getSwords();
			
			for(int i = 0; i < swords.size(); i++){
				
				Sword xy = (Sword) swords.get(i);
				g2d.drawImage(xy.getImage(), xy.getX(), xy.getY(), null);
					
			}
			
			if(myring.x > 400)
				if(enemy1.getAlive() == true)
					g2d.drawImage(enemy1.getImage(), enemy1.getX(), enemy1.getY(), null);
			
			if(myring.x > 600)
				if(enemy2.getAlive() == true)
					g2d.drawImage(enemy2.getImage(), enemy2.getX(), enemy2.getY(), null);
			
			if(myring.x > 1000)
				if(enemy3.getAlive() == true)
					g2d.drawImage(enemy3.getImage(), enemy3.getX(), enemy3.getY(), null);
			
			if(myring.x > 1400)
				if(enemy4.getAlive() == true)
					g2d.drawImage(enemy4.getImage(), enemy4.getX(), enemy4.getY(), null);
			
			if(myring.x > 1800)
				if(enemy5.getAlive() == true)
					g2d.drawImage(enemy5.getImage(), enemy5.getX(), enemy5.getY(), null);
			
			if(myring.x > 2000)
				if(enemy6.getAlive() == true)
					g2d.drawImage(enemy6.getImage(), enemy6.getX(), enemy6.getY(), null);
			
			//System.out.println(myring.getX() +" " + myring.bgx + " "+ myring.bgx2);
			
		}
		
		
		
		private class AL extends KeyAdapter{
			
			public void keyReleased(KeyEvent e){
				
				myring.keyReleased(e); 
			}
			
			public void keyPressed(KeyEvent e){
				
				myring.keyPressed(e);
			}
			
			
			
		}
}
