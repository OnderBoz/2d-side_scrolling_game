package myPrecious;

import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import javax.swing.ImageIcon;

public class Precious {
	
	int x, dx, y, dy, bgx, bgx2, leftBound;
	Image mychar;
	
	static ArrayList swords;
	
	public Precious(){
		
		ImageIcon i = new ImageIcon("images/precious.png");
		mychar = i.getImage();
		x = 75;
		bgx = 1240;
		bgx2 = 1240;
		y = 329;
		leftBound = 350;
		swords = new ArrayList();
		
	}
	
	public Rectangle getBound(){
		
		return new Rectangle(leftBound, y, 128, 128);
		
	}
	
	
	
	public static ArrayList getSwords(){
		
		return swords;
	}
	
	public void fire(){
		
		Sword xyz = new Sword((leftBound + 128), (y + 40));
		swords.add(xyz);
		
	}
	
	public void move(){
		if(dx != -2){
			
			if(leftBound + dx <=350){
				
				y = y + dy;
				leftBound = leftBound + dx;
			}
			
				
			 
			else{
			
				x = x + dx;
				y = y + dy;
				bgx = bgx + dx;
				bgx2 = bgx2 + dx;
				
			}
		}
			
		else{
			
			if(leftBound + dx > 0)
				leftBound = leftBound + dx;
				y = y + dy;
				
			}
		
		
		
	}
	
	public int getX(){
		
		return x;
	}
	
	public int getY(){
		
		return y;
	}
	
	public int getdx(){
		
		return dx;
	}
	
	public int getLeft(){
		
		return leftBound;
	}
	
	
	
	public Image getImage(){
		
		return mychar;
	}
	
	public void keyPressed(KeyEvent e){
		
		int key = e.getKeyCode();
		
		if(key == KeyEvent.VK_LEFT)
			dx = -2;
		
		if(key == KeyEvent.VK_RIGHT)
			dx = 2;
		
		if(key == KeyEvent.VK_UP)
			dy = -2;
		
		if(key == KeyEvent.VK_DOWN)
			dy = 2;
		
		if(key == KeyEvent.VK_SPACE)
			fire();
			
		
	
	}
	
	
	public void keyReleased(KeyEvent e){
		
		int key = e.getKeyCode();
		
		if(key == KeyEvent.VK_RIGHT);
			dx = 0;
		
		if(key == KeyEvent.VK_LEFT)
			dx = 0;
		
		if(key == KeyEvent.VK_UP)
			dy = 0;
		
		if(key == KeyEvent.VK_DOWN)
			dy = 0;
	
	}

}
